<?php
require_once __DIR__ . '/App.php';
function getApp() {
	static $app = NULL;
	
	if (empty($app)) {
		$app = new App(dirname(dirname(__FILE__)) . '/config/config.php');
	}
	
	return $app;
}

function appUrl($path = NULL) {
	return getApp()->getUrl($path);
}

function appPath($path = NULL) {
	return getApp()->getPath($path);
}

function render($path, $data = []) {
	extract($data);
	include dirname(__DIR__) . '/views/' . $path;
}
