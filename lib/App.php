<?php
require_once __DIR__ . '/php-activerecord/ActiveRecord.php';
require_once __DIR__ . '/Simple-PHP-Cache/cache.class.php';

class App {
	public $config;
	public $cache;

	// initialize app
	public function __construct($config_file) {
		$this->setConfig($config_file);
		$this->initCache();
		$this->initActiveRecord();
		session_start();
	}
	
	public function setConfig($config_file) {
		include $config_file;

		// config_file contains key 'config'
		$this->config = $config;
	}
	public function getConfig($name = NULL) {
		if ($name == NULL) {
			return $this->config;
		} else {
			return $this->config[$name];
		}
	}
	
	// cache functions
	public function initCache() {
		$this->cache = new Cache();
		$this->cache->setCachePath(dirname(__DIR__) . '/cache/');
	}
	
	public function getCacheItem($key) {
		$this->cache->eraseExpired();
		return $this->cache->retrieve($key);
	}
	public function setCacheItem($key, $value, $expiry = NULL) {
		$this->cache->eraseExpired();
		return $this->cache->store($key, $value, $expiry);
	}

	// activerecord functions
	public function initActiveRecord() {
		$ar_config = ActiveRecord\Config::instance();
		$ar_config->set_model_directory(__DIR__);
		$ar_config->set_connections(array(
			'default' => $this->getConfig('database')
		));
		$ar_config->set_default_connection('default');
	}

	// path / url functions
	public function getUrl($path = NULL) {
		if (empty($path)) {
			$path = '/';
		} elseif ($path[0] != '/') {
			$path = '/' . $path;
		}
		return $this->getConfig('baseurl') . $path;
	}
	
	public function getPath($path = NULL) {
		if (empty($path)) {
			$path = '/';
		} elseif ($path[0] != '/') {
			$path = '/' . $path;
		}
		return dirname(__DIR__) . $path;
	}
}
