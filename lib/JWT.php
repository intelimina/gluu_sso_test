<?php
require_once __DIR__ . '/common.php';

class JWT {
	public $raw;
	public $parsed;

	public function __construct($raw) {
		$this->raw = $raw;
		$this->parsed = $this->decode($raw);
	}

	// given a raw ID token, returns an array that contains [$header, $payload, $signature] of the token
	public static function decode($raw) {
		$strings = explode('.', $raw);
		$parsed = [];
		$parsed['header'] = json_decode(self::base64_urlsafe_decode($strings[0]), TRUE);
		if (json_last_error()) {
			throw new JWTGarbageException("Invalid token header: " . json_last_error_msg() . " while decoding {$this->raw}");
		}

		$parsed['payload'] = json_decode(self::base64_urlsafe_decode($strings[1]), TRUE);
		if (json_last_error()) {
			throw new JWTGarbageException("Invalid token payload: " . json_last_error_msg() . " while decoding {$this->raw}");
		}

		$parsed['signature'] = $strings[2];

		return $parsed;
	}

	// given a non-padded base64 string, pads it and runs base64_decode
	public static function base64_urlsafe_decode($string) {
		$remainder = strlen($string) % 4;
		if ($remainder) {
			$padding = 4 - $remainder;
			$string .= str_repeat('=', $padding);
		}

		return base64_decode(strtr($string, '-_', '+/'));
	}

	// given a bitstream, creates a url-safe base64_encoded string
	public static function base64_urlsafe_encode($string) {
		return str_replace('=', '', strtr(base64_encode($string), '+/', '-_'));
	}

	// returns a Unix timestamp in seconds, minus the timezone offset
	public static function getTimestamp($datetime = NULL) {
		$now = new DateTime($datetime);
		return $now->getTimestamp();
	}

	/**
	 * given a raw JWT, returns true if it is valid and false if not.
	 * $params['secret'] = (optional) secret used to sign JWT
	 * $params['keys'] = (optional) array of keys that may be used to sign token
	 * $params['iss'] = (optional) must match the 'iss' claim to pass
	 * $params['aud'] = (optional) must match one of the 'aud' claims to pass
	 * $params['exp'] = (optional) if the 'exp' claim is present...
	 *                  and === FALSE, the 'exp' claim is ignored
	 *                  and === TRUE or unspecified, the 'exp' claim must be
	 *                  greater than current datetime
	 *                  and a datetime format, the 'exp' claim must be greater
	 *                  than specified datetime to pass
	 * $params['nbf'] = (optional) same as exp, but only passes if the 'nbf' claim
	 *                  is less than specified datetime
	 */
	public static function validateToken($raw, $params = array()) {
		try {
			$parsed = self::decode($raw);
		} catch (JWTGarbageException $e) {
			return false;
		}

		// verify that provided params matches token contents
		$match_params = array(
			'iss', 'nonce', 'c_hash', 'at_hash',
			'iat', 'auth_time', 'azp'
		);
		foreach ($match_params as $name) {
			if (!empty($params[$name]) &&
			    $params[$name] != $parsed['payload'][$name]) {
				return false;
			}
		}

		// verify signature
		if (empty($params['secret'])) {
			$params['secret'] = '';
		}

		if (!self::validateSignature($raw, $params['secret'])) {
			return false;
		}

		// verify audience
		if (!empty($params['aud'])) {
			if (is_string($parsed['payload']['aud']) && $params['aud'] != $parsed['payload']['aud']) {
				return false;
			}
			if (is_array($parsed['payload']['aud']) && !in_array($params['aud'], $parsed['payload']['aud'])) {
				return false;
			}
		}

		// verify expiration
		if (!empty($parsed['payload']['exp'])) {
			if (!empty($params['exp'])) {
				if ($params['exp'] === TRUE) {
					if ($parsed['payload']['exp'] < self::getTimestamp()) {
						return false;
					}
				} else {
					if ($parsed['payload']['exp'] < $params['exp']) {
						return false;
					}
				}
			}
		}

		// verify not before
		if (!empty($parsed['payload']['nbf'])) {
			if (!empty($params['nbf'])) {
				if ($params['nbf'] === TRUE) {
					if ($parsed['payload']['nbf'] > self::getTimestamp()) {
						return false;
					} elseif ($parsed['payload']['nbf'] > $params['nbf']) {
						return false;
					}
				}
			}
		}

		return true;
	}

	// given a raw JWT, returns true if the signature is valid
	public static function validateSignature($raw, $secret = '') {
		try {
			$parsed = self::decode($raw);
		} catch (JWTGarbageException $e) {
			return false;
		}

		$strings = explode('.', $raw);
		$message = $strings[0] . '.' . $strings[1];

		$algorithm = $parsed['header']['alg'];
		$signature = $parsed['signature'];
		switch ($algorithm) {
			case 'none':
				return empty($signature);
			case 'HS256':
			case 'HS384':
			case 'HS512':
				$hash_alg = 'sha' . substr($algorithm, 2);
				return $signature == self::base64_urlsafe_encode(hash_hmac($hash_alg, $message, $secret, TRUE));
			case 'RS256': $openssl_alg = OPENSSL_ALGO_SHA256;
			case 'RS384': $openssl_alg = OPENSSL_ALGO_SHA384;
			case 'RS512': $openssl_alg = OPENSSL_ALGO_SHA512;
				return openssl_verify($message, $signature, $secret, $openssl_alg);
			default:
				throw new JWTUnsupportedAlgorithmException($algorithm);
		}
	}

	// runs JWT::validateToken() on self with params
	public function validate($params = array()) {
		return self::validateToken($this->raw, $params);
	}

	public function getHeader() {
		return $this->parsed['header'];
	}
	public function getClaims() {
		return $this->parsed['payload'];
	}
	public function getClaim($name) {
		return $this->parsed['payload'][$name];
	}
	public function getSignature() {
		return $this->parsed['signature'];
	}
}
