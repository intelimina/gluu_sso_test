<?php
// initialize the app with the config file
require_once dirname(__FILE__) . '/App.php';

// globlaly required libraries
$requires = array(
	'common',
	'JWTException',
	'JWT',
	'OicException',
	'OicSession',
);

foreach ($requires as $file) {
	require_once dirname(__FILE__) . '/' . $file . '.php';
}

$app = getApp();
