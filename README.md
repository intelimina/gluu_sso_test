SSO Test Application
====================

This application tests SSO connectivity / API of an OpenID Connect 1.0 server.

It walks a developer through the login / logout workflow in an OpenID Connect 1.0 appliction.

## Installing
### Requirements
- php 5.4 and higher
- curl
- openssl
- MySQL (or DB supported by [php-activerecord](http://www.phpactiverecord.org/) )

### Creating the database
The `oic_sessions.sql` describes a table for storing the OpenID connect session data. You need to create a database with this table.

    CREATE DATABASE sso_test;
    SOURCE oic_sessions.sql;

### Editing the config
Copy `config/config.php.sample` to `config/config.php`. Then edit the contents with the following details:

- baseurl: URL where this application will be installed
- database: connection details to the database
- client_id and client_secret: client credentials from the SSO server
