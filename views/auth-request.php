<?php
	render('head.php');
?>
<body class="authorization_request">
<div class="container">
	<p>You are not logged in. <a href="<?= $redirect_url ?>">Click here to proceed to the SSO login</a></p>

	<p>The above redirect will cause the browser to send the following request to the authorization URL:</p>
	<pre><code class="sso-code-block"><?= json_encode($oic_session->getAuthorizationQuery(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) ?></code></pre>

	<p>In URL form:</p>
	<pre><code class="sso-code-block"><?= $oic_session->getAuthorizationUrl(); ?></code></pre>

</div>
</body>
</html>
