<?php
	render('head.php');
?>
<body class="authorization_response">
<div class="container">
	<p><strong>The SSO Test Client</strong> received the following authorization response:</p>
	<pre><code class="sso-code-block"><?= json_encode($_REQUEST, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) ?></code></pre>

	<div class="token_validation">
		<h2>Auth Validation</h2>
		<p>To validate the above parameters, the <strong>SSO Test Client</strong> sent a <code>POST</code> request to the Token URL</p>
		<pre><code class="sso-code-block">Token URL: <?= $token_endpoint ?></code></pre>

		<div class="token_request">
			<h3>Token Request</h3>
			<p>The <code>POST</code> request must be authenticated using <code>HTTP Basic Authorization</code>. This is done by passing an <code>HTTP header</code> such as follows:</p> 
			<pre><code class="sso-code-block">Authorization: Basic <?= $client_auth ?></code></pre>

			<p>The authorization is generated using the <code>HTTP Basic</code> algorithm, or:</p>
			<pre><code class="sso-code-block">base64("client_id:client_secret")</code></pre>

			<p>Finally the token request included the following parameters:</p>
			<pre><code class="sso-code-block"><?= json_encode($access_token_query, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES); ?></code></pre>
		</div>

		<div class="token_response">
			<h3>Token Response</h3>
			<p>The <strong>SSO server</strong> gave the following response to the Token request:</p>
			<pre><code class="sso-code-block"><?= json_encode($token_response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) ?></code></pre>
		</div>
	<p>Login processed successfully. <a href="<?= appUrl() ?>">Click here to go back to the application.</a></p>
</div>
</body>
</html>
