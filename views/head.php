<?php
?>
<!DOCTYPE html>
<html>
<head>
	<title>SSO Test Application</title>
	
	<link href="//fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet" type="text/css" />
	<link href="<?= appUrl('assets/stylesheets/site.css') ?>" rel="stylesheet" type="text/css" />
	<script src="<?= appUrl('assets/javascripts/all.js') ?>"></script>
</head>
