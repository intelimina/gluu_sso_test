<?php
	render('head.php');
	$sso_name = OicSession::getClientConfig('name');
	$sso_url = OicSession::getClientConfig('url');
?>
<body class="index">
<div class="container">
	<h1>SSO Test Application</h1>
	<p>This application tests SSO connectivity / API for <a href="<?= $sso_url ?>"><?= $sso_name ?></a>.</p>

	<p>Not yet logged in. <a href="oic/login.php">Log in?</a></p>
</div>
</body>
</html>

