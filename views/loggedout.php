<?php
	render('head.php');
?>
<body class="loggedout">
<div class="container">
<p>You have been logged out. <a href="<?= appUrl('/oic/login.php') ?>">Click here to login again.</a></p>
</div>
</body>
</html>
