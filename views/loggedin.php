<?php
	render('head.php');
	$sso_name = OicSession::getClientConfig('name');
	$sso_url = OicSession::getClientConfig('url');
	$oic_session = OicSession::find($_SESSION['oic_session_id']);
	$claims = $oic_session->getClaims();
?>
<body class="loggedin">
<div class="container">
	<p>This application tests for SSO connectivity / API for <a href="<?= $sso_url ?>"><?= $sso_name ?></a>.</p>

	<p>Logged in as <em><?= $claims['given_name'] . ' ' . $claims['family_name']; ?></em></p>

	<p><a class="sso-cta" href="<?= appUrl('oic/logout.php') ?>">Log out?</a></p>
	<hr />

	<div id="SessionStatus">
		<h2>Session Status</h2>
		<table class="sso-status">
			<tbody>
				<tr>
					<th>Auth Time</th>
					<td><?= date('r', $claims['iat']); ?></td>
				</tr>
				<tr>
					<th>Expiry Time</th>
					<td><?= date('r', $claims['exp']); ?></td>
				</tr>
				<tr>
					<th>Raw ID Token</th>
					<td><?= $oic_session->id_token ?></td>
				</tr>
				<tr>
					<th>ID Token valid?</th>
					<td><?= ($oic_session->validateIdToken()) ? "Yes" : "No" ?></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div id="Claims">
		<h2>Session claims</h2>
		<pre><code class="sso-code-block"><?= json_encode($claims, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES); ?></code></pre>
	</div>
</div>
</body>
</html>
