<script type="text/javascript">
var wl = window.location;
var url;
if (wl.hash.length > 0) {
        url = wl.protocol + '//' + wl.hostname + (wl.port ? ':' + wl.port : '') + wl.pathname + '?' + wl.hash.substring(1);
        document.write('<p>SSO Client received authorization response in the URL hash. The SSO Client should normally use Javascript to redirect the user to server-side processing of the authorization response. Click <a href="' + url + '">here</a> to do so.</p>');
}
</script>
