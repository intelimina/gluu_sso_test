<?php
	require_once dirname(__FILE__) . '/lib/init.php';

	// try to retrieve from session
	if (!empty($_SESSION['oic_session_id'])) {
		try {
			$oic_session = OicSession::find($_SESSION['oic_session_id']);

			// do not include incomplete sessions
			if (!$oic_session->isComplete()) {
				$oic_session->delete();
				$oic_session = NULL;
			}
		} catch (ActiveRecord\RecordNotFound $e) {
			$oic_session = NULL;
		}
	} else {
		$oic_session = NULL;
	}

	if (empty($oic_session)) {
		render('not-loggedin.php');
	} else {
		render('loggedin.php');
	}
