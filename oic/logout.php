<?php
	require_once __DIR__ . '/../lib/init.php';
	if (empty($_SESSION['oic_session_id'])) {
		// no openid session, logout the local account
		session_destroy();
		render('loggedout.php');
		exit();
	}
	
	try {
		$oic_session = OicSession::find($_SESSION['oic_session_id']);
	} catch (ActiveRecord\RecordNotFound $e) {
		session_destroy();
		render('loggedout.php');
		exit();
	}
	
	header('Location: ' . $oic_session->getEndSessionUrl());
	$oic_session->delete();
	$session_destroy();
