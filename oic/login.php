<?php
	// called by client to redirect to gluu
	require_once __DIR__ . '/../lib/init.php';

	if (empty($_SESSION['oic_session_id'])) {
		$oic_session = new OicSession;
		$oic_session->save();
		
		$_SESSION['oic_session_id'] = $oic_session->attributes()['id'];
	} else {
		try {
			$oic_session = OicSession::find($_SESSION['oic_session_id']);
		} catch (ActiveRecord\RecordNotFound $e) {
			$oic_session = new OicSession;
		}

		if ($oic_session->isComplete() && $oic_session->isExpired()) {
			try {
				$oic_session->refreshAccessToken();
			} catch (OicErrorException $e) {
				$oic_session->delete();
				$oic_session = new OicSession;
			}
		}
		$oic_session->save();
		
		$_SESSION['oic_session_id'] = $oic_session->attributes()['id'];
	}
	
	if ($oic_session->isComplete()) {
		$logged_in = TRUE;
		// user already logged in
		$redirect_url = appUrl();
		$email = $oic_session->getClaim('email');
		$expires_at = $oic_session->attributes()['expires_at'];
		if (!empty($expires_at)) {
			$expiry = (new DateTime($expires_at))->format('U') * 1000;
		} else {
			$expiry = (new DateTime())->format('U') * 1000 + 300000;
		}
	} else {
		// user not yet logged in
		$logged_in = NULL;
		$redirect_url = $oic_session->getAuthorizationUrl();
	}
	
	if ($logged_in) {
		render('loggedin.php');
	} else {
		render('auth-request.php', [
			'redirect_url' => $redirect_url,
			'oic_session' => $oic_session,
		]);
	}
