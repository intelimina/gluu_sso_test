<?php
	// called by gluu to redirect to app
	require_once dirname(dirname(__FILE__)) . '/lib/init.php';
	
	// Establish a valid OpenID Connect Session
	if (empty($_SESSION['oic_session_id'])) {
		header("HTTP/1.1 401 Unauthorized");
		exit('<a href="' . appUrl('oic/login.php') . '">Client must login.</a>');
	}
	
	try {
		$oic_session = OicSession::find($_SESSION['oic_session_id']);
	} catch (ActiveRecord\RecordNotFound $e) {
		unset($_SESSION['oic_session_id']);
		header("HTTP/1.1 401 Unauthorized");
		exit('Login session expired. <a href="' . appUrl('/oic/logout.php') . '">Client must reauthorize.</a>');
	}
	
	if ($oic_session->isComplete() && $oic_session->isExpired()) {
		try {
			$oic_session->refreshAccessToken();
		} catch (OicErrorException $e) {
			$oic_session->delete();
			unset($_SESSION['oic_session_id']);
			header("HTTP/1.1 401 Unauthorized");
			exit('Login session expired. <a href="' . appUrl('/oic/logout.php') . '">Client must reauthorize.</a>');
		}
	}
	
	if (empty($_REQUEST['code'])) {
		render('login.php');
		exit();
	}
	
	if ($oic_session->attributes()['state'] != $_REQUEST['state']) {
		$oic_session->delete();
		unset($_SESSION['oic_session_id']);
		header("HTTP/1.1 401 Unauthorized");
		exit('Invalid OpenID Connect request. <a href="' . appUrl('/oic/login.php') . '">Click here to reauthorize.</a>');
	}
	
	$oic_session->set_attributes([
		'code' => $_REQUEST['code'],
		'session_state' => $_REQUEST['session_state'],
	]);
	if (!empty($_REQUEST['id_token'])) {
		$oic_session->set_attributes(['id_token' => $_REQUEST['id_token']]);
		if (!$oic_session->validateIdToken()) {
			$oic_session->delete();
			unset($_SESSION['oic_session_id']);
			header("HTTP/1.1 401 Unauthorized");
			exit('Invalid ID Token. <a href="' . appUrl('/oic/login.php') . '">Click here to reauthorize.');
		}
	}
	
	try {
		$access_token_query = array_filter($oic_session->getAccessTokenQuery());
		$token_response = $oic_session->getAccessToken();
	} catch (OicException $e) {
		$oic_session->delete();
		unset($_SESSION['oic_session_id']);
		header("HTTP/1.1 401 Unauthorized");
		exit('Error while retrieving authorization: ' . $e->getMessage() . '. <a href="' . appUrl('/oic/login.php') . '">Click here to reauthorize</a>');
	}
	
	// OpenID Connect session established
	
	// Create a local login session
	$expires_at = $oic_session->attributes()['expires_at'];
	if (!empty($expires_at)) {
		$expiry = (new DateTime($expires_at))->format('U') * 1000;
	} else {
		$expiry = (new DateTime())->format('U') * 1000 + 300000;
	}
	
	$oic_session->save();
	$client_id = $oic_session::getClientConfig('client_id');
	$client_secret = $oic_session::getClientConfig('client_secret');
	$client_auth = base64_encode("$client_id:$client_secret");
	$token_endpoint = $oic_session::getDynamicConfig('token_endpoint');

	render('auth-response.php', [
		'oic_session' => $oic_session,
		'access_token_query' => $access_token_query,
		'token_response' => $token_response,
		'token_endpoint' => $token_endpoint,
		'client_id' => $client_id,
		'client_secret' => $client_secret,
		'client_auth' => $client_auth,
	]);
